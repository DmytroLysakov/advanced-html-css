
export function isWebp() {
	function testWebP(callback) {
		let webP = new Image();
		webP.onload = webP.onerror = function () {
			callback(webP.height == 2);
		};
		webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
	}

	testWebP(function (support) {
		let className = support === true ? 'webp' : 'no-webp';
		document.documentElement.classList.add(className);
	});
}


// export function headerMenuAction() {
// 	const burger = document.querySelector('.burger');
// 	const nav = document.querySelector('.nav');
// 	const header = document.querySelector('.header')
// 	// Open/Close mobile menu
// 	header.addEventListener('click', toggleNavMenu);
// 	function toggleNavMenu(e) {
// 		if (e.target.closest('.burger') || e.target.closest('.nav')) {
// 			burger.classList.toggle('burger--active');
// 			nav.classList.toggle('nav--open')
// 		}
// 	}
// }

const burger = document.querySelector('.burger');
const menu = document.querySelector('.nav');

export function handlerBurger() {
	burger.classList.toggle('burger--active');
	menu.classList.toggle('nav--open');
}

burger.addEventListener('click', handlerBurger);


